import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:prueba1/models/login.model.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:prueba1/services/storage.service.dart';

class HomeController extends GetxController with StateMixin<dynamic> {
  List<String> modelBikes = ["Trek Powerfly FS4", "Megamo Crave CRB 05"];
  RxBool isFormFilled = false.obs;
  RxBool isUploading = false.obs;
  bool fileExists = false;
  RxBool isLoading = false.obs;
  RxBool isLoaded = false.obs;
  Rx<BicisFile> testData = BicisFile(
    detalle: '',
    fecha: '1',
    localizacionId: 0,
    operacion: [],
    precio: 0,
    recambioId: 0,
    taller: '',
    modelo: '',
    image: '',
  ).obs;

  RxList<BicisFile> arrayFiles = <BicisFile>[].obs;
  Rx<Future<firebase_storage.ListResult>> resultFiles =
      StorageService().listFiles().obs;

  Rx<Future<firebase_storage.ListResult>> reloadData() {
    isLoaded.value = false;
    resultFiles.value = StorageService().listFiles();
    return resultFiles;
  }

  @override
  void onInit() {
    super.onInit();
    reloadData();
    hydrateData();
  }

  hydrateData() async {
    isLoading.value = true;
    arrayFiles.clear();
    final jsonList = await resultFiles.value;
    for (var file in jsonList.items) {
      if (file.name.contains('.json')) {
        final response = await StorageService().downloadJSON(file.name);
        if (response == null) {
          debugPrint('Error while downloading file : ${response.error}');
        } else {
          testData.value = BicisFile.fromMap(
                  jsonDecode(String.fromCharCodes(response).toString()))
              .obs
              .value;
          isLoaded.value = true;
          testData.value.modelo =
              file.name.contains('CRAVE') ? modelBikes[1] : modelBikes[0];
          testData.value.image =
              !file.name.contains('CRAVE') ? 'bike2.png' : 'bike1.png';
          arrayFiles.add(testData.value);
          isLoading.value = false;
          refresh();
        }
      }
    }
  }

  getModel(String model) {
    String? d =
        modelBikes.firstWhere((e) => e.contains(model), orElse: () => 'none');
    return d;
  }
}
