import 'package:get/get.dart';

class LoginController extends GetxController with StateMixin<dynamic> {
  var username = ''.obs;
  RxBool isLoading = false.obs;
  RxString password = ''.obs;
  RxList data = [].obs;

  setUsername(String userRecieved) {
    username.value = userRecieved;
  }

  setPassword(String passRecieved) {
    password.value = passRecieved;
  }
}
