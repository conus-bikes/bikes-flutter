import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:prueba1/controllers/home_controller.dart';
import 'package:prueba1/models/login.model.dart';
import 'package:prueba1/services/storage.service.dart';

class ReactiveWidgetsController extends GetxController
    with StateMixin<dynamic> {
  final HomeController homeController = Get.put(HomeController());
  RxBool isLoading = false.obs;
  RxBool isLoaded = false.obs;
  RxList<BicisFile> arrayFiles = <BicisFile>[].obs;
  RxBool isFormShowed = false.obs;
  Rx<BicisFile> singleBike = BicisFile(
    detalle: '',
    fecha: '1',
    localizacionId: 0,
    operacion: [],
    precio: 0,
    recambioId: 0,
    taller: '',
    modelo: '',
    image: '',
  ).obs;

  List<String> modelBikes = ["Trek Powerfly FS4", "Megamo Crave CRB 05"];
  RxString selectedValueVehicles = "".obs;
  RxString selectedValueOperaciones = "".obs;
  RxString selectedValueComponents = "".obs;
  RxString selectedValueLocalization = "".obs;
  RxString selectedValueReplacements = "".obs;
  RxString selectedValuePrice = "".obs;
  RxString selectedValueKms = "".obs;
  RxString selectedValueTaller = "".obs;
  RxString selectedValueDescripcion = "".obs;
  RxBool isUploading = false.obs;
  RxBool isFilled = false.obs;
  bool fileExists = false;
  final descController = TextEditingController();
  final precioController = TextEditingController();
  final tallerController = TextEditingController();
  final kmsController = TextEditingController();
  final StorageService storage = StorageService();
  Rx<Future<firebase_storage.ListResult>> resultFiles =
      StorageService().listFiles().obs;
  Rx<Future<firebase_storage.ListResult>> reloadData() {
    resultFiles.value = StorageService().listFiles();
    return resultFiles;
  }

  String convertDateFromString(String fecha) {
    return '${fecha.substring(6)}/${fecha.substring(4, fecha.length - 2)}/${fecha.substring(0, 4)}';
  }

  bool checkFilledForm(String modelo) {
    if (modelo.contains('Crave')) {
      selectedValueVehicles.value = 'CRAVE05';
    } else {
      selectedValueVehicles.value = 'TREKFS04';
    }
    if (selectedValueOperaciones.value != '' &&
        selectedValueComponents.value != '' &&
        selectedValueLocalization.value != '' &&
        selectedValueReplacements.value != '' &&
        descController.text != '' &&
        precioController.text != '' &&
        tallerController.text != '' &&
        kmsController.text != '') {
      isFilled.value = true;
      return true;
    }
    isFilled.value = false;
    return false;
  }

  uploadFile() async {
    isUploading.value = true;

    final Map<String, dynamic> jsonToFile = {
      "Fecha": DateFormat('yyyyMMdd').format(DateTime.now()),
      "Operacion": [
        {
          "Tipo": selectedValueOperaciones.value,
          "Zona": "",
          "Kms": int.parse(selectedValueKms.value)
        }
      ],
      "Detalle": selectedValueDescripcion.value,
      "Localizacion_id": int.parse(selectedValueLocalization.value),
      "Recambio_id": int.parse(selectedValueReplacements.value),
      "Precio": int.parse(selectedValuePrice.value),
      "Taller": selectedValueTaller.value
    };
    await Future.delayed(const Duration(seconds: 2), () {});

    final file = await _localFile;
    file.writeAsStringSync(json.encode(jsonToFile));
    await storage.uploadFile(file.path, '${selectedValueVehicles.value}.json');
    isUploading.value = false;
    initValues();
    reloadData();
    hydrateData();
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/counter.json');
  }

  initValues() {
    selectedValueVehicles = "".obs;
    selectedValueOperaciones = "".obs;
    selectedValueComponents = "".obs;
    selectedValueLocalization = "".obs;
    selectedValueReplacements = "".obs;
    selectedValuePrice = "".obs;
    selectedValueKms = "".obs;
    selectedValueTaller = "".obs;
    selectedValueDescripcion = "".obs;
    descController.text = '';
    precioController.text = '';
    tallerController.text = '';
    kmsController.text = '';
    isFilled = false.obs;
  }

  hydrateData() async {
    isLoading.value = true;
    arrayFiles.clear();
    final jsonList = await resultFiles.value;
    for (var file in jsonList.items) {
      if (file.name.contains('.json')) {
        final response = await StorageService().downloadJSON(file.name);
        if (response == null) {
          debugPrint('Error while downloading file : ${response.error}');
        } else {
          singleBike.value = BicisFile.fromMap(
                  jsonDecode(String.fromCharCodes(response).toString()))
              .obs
              .value;
          isLoaded.value = true;
          singleBike.value.modelo =
              file.name.contains('CRAVE') ? modelBikes[1] : modelBikes[0];
          singleBike.value.image =
              file.name.contains('CRAVE') ? 'bike2.png' : 'bike1.png';
          arrayFiles.add(singleBike.value);
          isLoading.value = false;
        }
      }
    }
  }
}
