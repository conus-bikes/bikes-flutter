import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart' as firebase_core;
import 'package:flutter/material.dart';

class StorageService {
  final firebase_storage.FirebaseStorage storage =
      firebase_storage.FirebaseStorage.instance;

  Future<void> uploadFile(
    String filePath,
    String fileName,
  ) async {
    File file = File(filePath);
    try {
      await storage.ref('transient-data/$fileName').putFile(file);
    } on firebase_core.FirebaseException catch (e) {
      debugPrint('Error from FBase: $e');
    }
  }

  Future<firebase_storage.ListResult> listFiles() async {
    firebase_storage.ListResult results =
        await storage.ref('transient-data').listAll();
    return results;
  }

  Future<String> downloadURL(String imageName) async {
    String downloadURL =
        await storage.ref('transient-data/$imageName').getDownloadURL();
    return downloadURL;
  }

  downloadJSON(String filename) async {
    return await storage.ref('transient-data/$filename').getData(10485759);
  }
}
