import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:prueba1/bindings/home_bindings.dart';
import 'package:prueba1/pages/home_page.dart';
import 'package:prueba1/pages/login_page.dart';
import 'package:prueba1/pages/register_page.dart';

import 'bindings/login_bindings.dart';

void main() async {
  await GetStorage.init();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    GetMaterialApp(
      theme: ThemeData(
        backgroundColor: Colors.white,
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/login',
      initialBinding: LoginBinding(),
      getPages: [
        GetPage(
          name: '/login',
          page: () => LoginPage(),
          binding: LoginBinding(),
        ),
        GetPage(
          name: '/home',
          page: () => const HomePage(),
          binding: HomeBinding(),
        ),
        GetPage(
          name: '/register',
          page: () => RegisterPage(),
          binding: LoginBinding(),
        ),
      ],
    ),
  );
}

isValidtoken() {
  final mytoken = GetStorage().read('token');
  return JwtDecoder.isExpired(mytoken);
}
