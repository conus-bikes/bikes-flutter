// To parse this JSON data, do
//
//     final albumModel = albumModelFromMap(jsonString);

import 'dart:convert';

class LoginResponse {
  LoginResponse({required this.token});

  final String token;

  factory LoginResponse.fromJson(String str) =>
      LoginResponse.fromMap(json.decode(str));
  factory LoginResponse.toMap(String str) =>
      LoginResponse.toMap(json.encode(str));

  String toJson() => json.encode(toMap());

  factory LoginResponse.fromMap(Map<String, dynamic> json) =>
      LoginResponse(token: json["token"]);

  Map<String, dynamic> toMap() => {"token": token};
}

// To parse this JSON data, do
//
//     final bicisFile = bicisFileFromMap(jsonString);

class BicisFile {
  BicisFile(
      {required this.fecha,
      required this.operacion,
      required this.detalle,
      required this.localizacionId,
      required this.recambioId,
      required this.precio,
      required this.taller,
      required this.modelo,
      required this.image});

  final String fecha;
  final List<Operacion> operacion;
  final String detalle;
  final int localizacionId;
  final int recambioId;
  final double precio;
  final String taller;
  String modelo;
  String image;

  factory BicisFile.fromJson(String str) => BicisFile.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory BicisFile.fromMap(Map<String, dynamic> json) => BicisFile(
      fecha: json["Fecha"],
      operacion: List<Operacion>.from(
          json["Operacion"].map((x) => Operacion.fromMap(x))),
      detalle: json["Detalle"],
      localizacionId: json["Localizacion_id"],
      recambioId: json["Recambio_id"],
      precio: json["Precio"].toDouble(),
      taller: json["Taller"],
      modelo: '',
      image: '');

  Map<String, dynamic> toMap() => {
        "Fecha": fecha,
        "Operacion": List<dynamic>.from(operacion.map((x) => x.toMap())),
        "Detalle": detalle,
        "Localizacion_id": localizacionId,
        "Recambio_id": recambioId,
        "Precio": precio,
        "Taller": taller,
        "Modelo": modelo,
      };
}

class Operacion {
  Operacion({
    required this.tipo,
    required this.zona,
    required this.kms,
  });

  final String tipo;
  final String zona;
  final int kms;

  factory Operacion.fromJson(String str) => Operacion.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Operacion.fromMap(Map<String, dynamic> json) => Operacion(
        tipo: json["Tipo"],
        zona: json["Zona"],
        kms: json["Kms"],
      );

  Map<String, dynamic> toMap() => {
        "Tipo": tipo,
        "Zona": zona,
        "Kms": kms,
      };
}
