import 'package:get/get.dart';
import 'package:prueba1/controllers/reactive_widgets_controller.dart';

class ReactiveWidgetsBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ReactiveWidgetsController>(() => ReactiveWidgetsController());
  }
}
