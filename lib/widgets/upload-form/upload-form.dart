import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:prueba1/controllers/home_controller.dart';
import 'package:prueba1/controllers/reactive_widgets_controller.dart';
import 'package:prueba1/models/login.model.dart';
import 'package:prueba1/widgets/dropdown_components.dart';
import 'package:prueba1/widgets/dropdown_localizations.dart';
import 'package:prueba1/widgets/dropdown_operations.dart';
import 'package:prueba1/widgets/dropdown_replacements.dart';

final ReactiveWidgetsController controller =
    Get.put(ReactiveWidgetsController());
final HomeController homeController = Get.put(HomeController());

Column formUpload(BicisFile bikemodel) {
  return Column(
    children: [
      Obx(
        () => DropdownButton(
          value: controller.selectedValueOperaciones.value,
          items: dropdownItemsOperaciones,
          onChanged: (String? value) {
            controller.selectedValueOperaciones.value = value.toString();
          },
        ),
      ),
      Obx(
        () => DropdownButton(
          value: controller.selectedValueComponents.value,
          items: dropdownItemsComponents,
          onChanged: (String? value) {
            controller.selectedValueComponents.value = value.toString();
          },
        ),
      ),
      Obx(
        () => DropdownButton(
          value: controller.selectedValueLocalization.value,
          items: dropdownItemsLocalizaciones,
          onChanged: (String? value) {
            controller.selectedValueLocalization.value = value.toString();
          },
        ),
      ),
      Obx(
        () => DropdownButton(
          value: controller.selectedValueReplacements.value,
          items: dropdownItemsReplacements,
          onChanged: (String? value) {
            controller.selectedValueReplacements.value = value.toString();
          },
        ),
      ),
      const SizedBox(
        height: 10,
      ),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: TextFormField(
          controller: controller.descController,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            labelText: "Descripción",
            prefixIcon: const Icon(Icons.description),
          ),
          keyboardType: TextInputType.name,
          onSaved: (v) => controller.checkFilledForm(bikemodel.modelo),
          onChanged: (value) {
            controller.selectedValueDescripcion.value = value.toString();
            controller.checkFilledForm(bikemodel.modelo);
          },
        ),
      ),
      const SizedBox(
        height: 10,
      ),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: TextFormField(
          controller: controller.precioController,
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              labelText: "Precio",
              prefixIcon: const Icon(Icons.description),
              suffixText: '€'),
          keyboardType: TextInputType.number,
          onSaved: (v) => controller.checkFilledForm(bikemodel.modelo),
          onChanged: (value) {
            controller.selectedValuePrice.value = value.toString();
            controller.checkFilledForm(bikemodel.modelo);
          },
        ),
      ),
      const SizedBox(
        height: 10,
      ),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: TextFormField(
          controller: controller.tallerController,
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              labelText: "Taller",
              prefixIcon: const Icon(Icons.pedal_bike_sharp)),
          keyboardType: TextInputType.name,
          onSaved: (v) => controller.checkFilledForm(bikemodel.modelo),
          onChanged: (value) {
            controller.selectedValueTaller.value = value.toString();
            controller.checkFilledForm(bikemodel.modelo);
          },
        ),
      ),
      const SizedBox(
        height: 10,
      ),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: TextFormField(
          controller: controller.kmsController,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            labelText: "Kms",
            prefixIcon: const Icon(Icons.add_road_sharp),
          ),
          keyboardType: TextInputType.number,
          onSaved: (v) => controller.checkFilledForm(bikemodel.modelo),
          onChanged: (value) {
            controller.selectedValueKms.value = value.toString();
            controller.checkFilledForm(bikemodel.modelo);
          },
        ),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
            child: Obx(
              () => controller.isFilled.value == true
                  ? Obx(
                      () => controller.isUploading.value == false
                          ? ElevatedButton(
                              onPressed: () async {
                                await controller.uploadFile();
                                homeController.onInit();
                                Get.back();
                              },
                              child: const Text('Update content...'))
                          : Row(
                              children: const [
                                Padding(
                                  padding: EdgeInsets.only(top: 20),
                                  child: CircularProgressIndicator(),
                                )
                              ],
                            ),
                    )
                  : const SizedBox(),
            ),
          ),
        ],
      ),
    ],
  );
}

Column bikeInfoDetail(BicisFile bikemodel) {
  return Column(
    children: [
      Row(
        mainAxisSize: MainAxisSize.min,
        children: const <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                'Information',
                style: TextStyle(fontSize: 20),
              ),
            ),
          ),
          Expanded(
            child: Padding(padding: EdgeInsets.all(20.0)),
          ),
        ],
      ),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Expanded(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                'Fecha',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                controller.convertDateFromString(bikemodel.fecha),
                style: const TextStyle(fontSize: 15),
              ),
            ),
          ),
        ],
      ),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Expanded(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                'Tipo de operación',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                bikemodel.operacion[0].tipo,
                style: const TextStyle(fontSize: 15),
              ),
            ),
          ),
        ],
      ),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Expanded(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                'Zona operación',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                bikemodel.operacion[0].zona,
                style: const TextStyle(fontSize: 15),
              ),
            ),
          ),
        ],
      ),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Expanded(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                'Kms operación',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                bikemodel.operacion[0].kms.toString(),
                style: const TextStyle(fontSize: 15),
              ),
            ),
          ),
        ],
      ),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Expanded(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                'Detalle',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                bikemodel.detalle,
                style: const TextStyle(fontSize: 15),
              ),
            ),
          ),
        ],
      ),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Expanded(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                'Localización Id',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                bikemodel.localizacionId.toString(),
                style: const TextStyle(fontSize: 15),
              ),
            ),
          ),
        ],
      ),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Expanded(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                'Recambio id',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                bikemodel.recambioId.toString(),
                style: const TextStyle(fontSize: 15),
              ),
            ),
          ),
        ],
      ),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Expanded(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                'Precio',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                '${bikemodel.precio.toString()} €',
                style: const TextStyle(fontSize: 15),
              ),
            ),
          ),
        ],
      ),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Expanded(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                'Taller',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Text(
                bikemodel.taller,
                style: const TextStyle(fontSize: 15),
              ),
            ),
          ),
        ],
      ),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: ElevatedButton(
                onPressed: () => controller.isFormShowed.value = true,
                child: const Text('Actualizar datos...'),
              ),
            ),
          ),
        ],
      ),
    ],
  );
}
