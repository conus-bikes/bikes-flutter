import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:prueba1/controllers/reactive_widgets_controller.dart';
import 'package:prueba1/models/login.model.dart';
import 'package:prueba1/widgets/title_item.dart';
import 'package:prueba1/widgets/upload-form/upload-form.dart';

class CardBike extends GetWidget<ReactiveWidgetsController> {
  final BicisFile bikemodel;
  const CardBike({Key? key, required this.bikemodel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        titleSpacing: 0.0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.black,
              ),
              onPressed: () {
                controller.initValues();
                controller.homeController.onInit();
                Get.back();
              },
            ),
            const Text(
              'Volver',
              style: TextStyle(color: Colors.black),
            )
          ],
        ),
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TitleItemDetail(text: bikemodel.modelo),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Image(
                      image: loadImageWidget(),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 25),
                    child: Divider(
                      height: 2,
                      color: Colors.black,
                    ),
                  ),
                  Obx(
                    () => !controller.isFormShowed.value
                        ? bikeInfoDetail(bikemodel)
                        : formUpload(bikemodel),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  AssetImage loadImageWidget() {
    String imageSrc = '';
    if (bikemodel.image == 'bike1.png') {
      imageSrc = 'bike1.png';
    } else {
      imageSrc = 'bike2.png';
    }
    return AssetImage('assets/$imageSrc');
  }
}
