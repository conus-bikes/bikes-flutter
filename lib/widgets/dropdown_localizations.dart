import 'package:flutter/material.dart';

List<DropdownMenuItem<String>> get dropdownItemsLocalizaciones {
  List<DropdownMenuItem<String>> menuItemsLocalizaciones = [
    const DropdownMenuItem(
        child: Text("Selecciona una localización"), value: ""),
    const DropdownMenuItem(child: Text("Delante"), value: "1"),
    const DropdownMenuItem(child: Text("Detrás"), value: "2"),
    const DropdownMenuItem(child: Text("Derecha"), value: "3"),
    const DropdownMenuItem(child: Text("Izquierda"), value: "4"),
  ];
  return menuItemsLocalizaciones;
}
