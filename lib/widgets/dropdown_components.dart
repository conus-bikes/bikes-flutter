import 'package:flutter/material.dart';

List<DropdownMenuItem<String>> get dropdownItemsComponents {
  List<DropdownMenuItem<String>> menuItemsComponents = [
    const DropdownMenuItem(child: Text("Selecciona un componente"), value: ""),
    const DropdownMenuItem(child: Text("Cadena"), value: "Cadena"),
    const DropdownMenuItem(child: Text("Frenos"), value: "Frenos")
  ];
  return menuItemsComponents;
}
