import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:prueba1/common/colors_bicis.dart';
import 'package:prueba1/controllers/home_controller.dart';
import 'package:prueba1/controllers/reactive_widgets_controller.dart';
import 'package:prueba1/widgets/reactive-widgets/bike_card.dart';

Widget tarjetasBicis(BuildContext context, HomeController controller) {
  final ReactiveWidgetsController rxController = Get.put(
    ReactiveWidgetsController(),
  );

  if (controller.isLoaded.value == false) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
  if (controller.isLoaded.value == true && controller.arrayFiles.isEmpty) {
    return const Text('no hay datos');
  }
  return SizedBox(
    width: MediaQuery.of(context).size.width,
    height: 380,
    child: Obx(
      () => ListView.separated(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: controller.arrayFiles.length,
        itemBuilder: (context, index) {
          rxController.singleBike.value = controller.arrayFiles[index];
          return SizedBox(
            width: MediaQuery.of(context).size.width * 0.8,
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.7,
                    child: Card(
                      color: Colors.white,
                      elevation: 10,
                      shadowColor: blueCardColor,
                      margin: const EdgeInsets.all(20),
                      shape: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                        borderSide:
                            const BorderSide(color: Colors.white, width: 5),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children:
                            rowsCardDetail(controller, index, rxController),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 13,
                  left: 10,
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                    child: SizedBox(
                      height: 50,
                      width: MediaQuery.of(context).size.width * 0.603,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(30),
                              bottomRight: Radius.circular(30),
                            ),
                          ),
                        ),
                        onPressed: () {
                          Get.to(
                            () => CardBike(
                                bikemodel: controller.arrayFiles[index]),
                          );
                        },
                        child: const Text(
                          'Ver detalle...',
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  left: MediaQuery.of(context).size.width * 0.1,
                  child: Obx(
                    () => Image(
                      image: AssetImage(
                          'assets/${controller.arrayFiles[index].image}'),
                      width: MediaQuery.of(context).size.width * 0.5,
                    ),
                  ),
                ),
              ],
            ),
          );
        },
        separatorBuilder: (context, index) {
          return const Divider(
            color: Colors.black,
          );
        },
      ),
    ),
  );
}

List<Widget> rowsCardDetail(HomeController controller, int index,
    ReactiveWidgetsController rxController) {
  return [
    Padding(
      padding: const EdgeInsets.only(top: 100, bottom: 15),
      child: Obx(
        () => Text(
          controller.arrayFiles[index].modelo,
          style: const TextStyle(fontSize: 20),
        ),
      ),
    ),
    Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Obx(
        () => Text(
          'Fecha: ${rxController.convertDateFromString(controller.arrayFiles[index].fecha)}',
          style: const TextStyle(fontSize: 15),
        ),
      ),
    ),
    Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Obx(
        () => Text(
          'Kms operacion: ${controller.arrayFiles[index].operacion[0].kms}',
          style: const TextStyle(fontSize: 15),
        ),
      ),
    ),
    Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Obx(
        () => Text(
          'Precio: ${controller.arrayFiles[index].precio}',
          style: const TextStyle(fontSize: 15),
        ),
      ),
    ),
    Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Obx(
        () => Text(
          'Taller: ${controller.arrayFiles[index].taller}',
          style: const TextStyle(fontSize: 15),
        ),
      ),
    )
  ];
}
