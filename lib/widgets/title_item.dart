import 'package:flutter/material.dart';

class TitleItemDetail extends StatelessWidget {
  final String text;
  const TitleItemDetail({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: const TextStyle(
          fontSize: 25, color: Colors.black, fontWeight: FontWeight.bold),
    );
  }
}
