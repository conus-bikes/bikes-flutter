import 'package:flutter/material.dart';

List<DropdownMenuItem<String>> get dropdownItems {
  List<DropdownMenuItem<String>> menuItems = [
    const DropdownMenuItem(child: Text("Selecciona un vehículo"), value: ""),
    const DropdownMenuItem(child: Text("Megamo Crave 05"), value: "CRAVE05"),
    const DropdownMenuItem(child: Text("Trek Powerfly FS04"), value: "TREKFS04")
  ];
  return menuItems;
}
