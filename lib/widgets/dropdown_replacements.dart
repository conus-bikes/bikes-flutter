import 'package:flutter/material.dart';

List<DropdownMenuItem<String>> get dropdownItemsReplacements {
  List<DropdownMenuItem<String>> menuItemsReplacements = [
    const DropdownMenuItem(child: Text("Selecciona un recambio"), value: ""),
    const DropdownMenuItem(child: Text("Replace1"), value: "1"),
    const DropdownMenuItem(child: Text("Replace2"), value: "2")
  ];
  return menuItemsReplacements;
}
