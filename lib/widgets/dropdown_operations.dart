import 'package:flutter/material.dart';

List<DropdownMenuItem<String>> get dropdownItemsOperaciones {
  List<DropdownMenuItem<String>> menuItemsOperaciones = [
    const DropdownMenuItem(child: Text("Selecciona una operación"), value: ""),
    const DropdownMenuItem(child: Text("Medición"), value: "Medir"),
    const DropdownMenuItem(child: Text("Cambio de pieza"), value: "Cambiar")
  ];
  return menuItemsOperaciones;
}
