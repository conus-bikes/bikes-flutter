import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:prueba1/controllers/home_controller.dart';
import 'package:prueba1/widgets/tarjetas_detail.dart';
import '../widgets/header_widget.dart';

class HomePage extends GetWidget<HomeController> {
  const HomePage({Key? key}) : super(key: key);
  final double _headerHeight = 150;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton(
        onPressed: () => reloadData(),
        heroTag: 'fab_data',
        child: const Icon(Icons.refresh_rounded, color: Colors.white),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: _headerHeight,
                  //este HeaderWidget lo hemos creado nosotros en la carpeta widgets para que se pueda usar en todas las pantallas
                  child: HeaderWidget(_headerHeight),
                ),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: SizedBox(
                    child: Text(
                      'Tus bicis'.toUpperCase(),
                      textAlign: TextAlign.start,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: const TextStyle(
                          fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 25.0),
                  child: Obx(
                    () => tarjetasBicis(context, controller),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  reloadData() {
    controller.reloadData();
    controller.hydrateData();
  }
}
