import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:prueba1/common/theme_helper.dart';

import '../controllers/login_controller.dart';
import '../widgets/header_widget.dart';

class RegisterPage extends GetWidget<LoginController> {
  RegisterPage({Key? key}) : super(key: key);

  final double _headerHeight = 220;
  final Key _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double widthContainer = MediaQuery.of(context).size.width;
    // La siguiente linea se usaría si quisieramos inyectar otro controlador que no esta definido para esta View
    // final HelperController helperController = Get.put(HelperController());
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: _headerHeight,
              //este HeaderWidget lo hemos creado nosotros en la carpeta widgets para que se pueda usar en todas las pantallas
              child: HeaderWidget(_headerHeight),
            ),
            SafeArea(
              child: Container(
                padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Column(
                  children: [
                    const Text(
                      'Bicis',
                      style:
                          TextStyle(fontSize: 60, fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                      child: Text(
                        'Registrar Usuario',
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                    Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          Container(
                            child: TextField(
                              onChanged: (user) => controller.setUsername(user),
                              decoration: ThemeHelper().textInputDecoration(
                                  'Usuario', 'ejemplo@domain.es'),
                            ),
                            decoration:
                                ThemeHelper().inputBoxDecorationShaddow(),
                          ),
                          const SizedBox(height: 30.0),
                          Container(
                            child: TextField(
                              onChanged: (pass) => controller.setPassword(pass),
                              obscureText: true,
                              decoration: ThemeHelper()
                                  .textInputDecoration('Contraseña', 'fwXybwt'),
                            ),
                            decoration:
                                ThemeHelper().inputBoxDecorationShaddow(),
                          ),
                          const SizedBox(height: 15.0),
                          Obx(
                            () => (controller.isLoading.value)
                                ? const CircularProgressIndicator()
                                : Container(
                                    width: widthContainer,
                                    decoration: ThemeHelper()
                                        .buttonBoxDecoration(context),
                                    child: ElevatedButton(
                                      style: ThemeHelper().buttonStyle(),
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            40, 10, 40, 10),
                                        child: Text(
                                          'Regístrame'.toUpperCase(),
                                          style: const TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                      ),
                                      onPressed: () => Get.offNamedUntil(
                                          '/home', (route) => false),
                                    ),
                                  ),
                          ),
                          Container(
                            margin: const EdgeInsets.fromLTRB(10, 20, 10, 20),
                            child: Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                      text: "Ya tienes una cuenta? "),
                                  TextSpan(
                                    text: 'Loguéate',
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        Get.toNamed('/login');
                                      },
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
